# vue2+webpack5 最新脚手架

## 功能

- [x] vue2.7 + element-ui 生态
- [x] webpack5 (webpack5， webpack-cli, webpack-dev-server) 持续更新优化
- [x] chunk 优化, 已经针对通用模块进行基础分包
- [x] babel-loader缓存
- [x] oxlint 代码格式化，语法验证
- [x] sass/less/husky/babel/prettier 均为最新，可持续迭代升级
- ... 等

## 脚本说明

```
npm run serve  开发环境

npm run build 生产打包

npm run analyzer 生产构建 + 项目分析文件输出

npm run lint   基于oxlint 进行代码语法修复
```

## node说明

当前运行在 node: 14.21.3 和18.19.0, 20.10.0 正常。建议使用20.10.0 因为lint-stage等包依赖高版本的ESM

14.21.3 在提交的时候，使用了 lint-staged ，这个包依赖高版本的node， 所以会提交不上去， 报如下错误，不影响webpack周边

```
static {
         ^
SyntaxError: Unexpected token '{'
    at Loader.moduleStrategy (internal/modules/esm/translators.js:149:18)
```

### 相关包

- bundle-stats-webpack-plugin 分析依赖
- webpack-bundle-analyzer 分析依赖的

