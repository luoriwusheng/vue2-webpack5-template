import chalk from 'chalk'
console.log('')
console.log(chalk.bgGreenBright('**************** lint-staged启动 ********************'))
console.log('')

export default {
  '*.js': ['prettier --write', 'oxlint --fix'],
  '*.jsx': ['prettier --write', 'oxlint --fix'],
  '*.ts': ['prettier --write', 'oxlint --fix'],
  '*.vue': ['prettier --write', 'oxlint --fix'],
  '**/*.(scss,less,css)': ['prettier --write']
}
