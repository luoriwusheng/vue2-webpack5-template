/**
 * @type {import('@babel/core').TransformOptions}
 */
module.exports = {
  // 目前有问题,不能在根目录配置 babel.config.js文件，一配置就报错，所有的babel都配置在这里即可
  presets: [
    [
      "@babel/preset-env"
    ],
    // vue2 支持jsx
    '@vue/babel-preset-jsx'
  ],
  plugins: [
    // [
    //   // element-ui 懒加载
    //   // babel-plugin-component
    //   "component",
    //   {
    //     libraryName: "element-ui",
    //     styleLibraryName: "theme-chalk"
    //   }
    // ]
  ]
}