const { loadEnv } = require('../utils/loadEnv')

const path = require('path')
const { VueLoaderPlugin } = require('vue-loader')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require('webpack')

/**
 * @type {import('webpack').Configuration}
 */
exports.default = {
  entry: './src/main.js',

  plugins: [
    // 提供进度条
    new webpack.ProgressPlugin(),

    new VueLoaderPlugin(),

    // 自动在dist目录生成 index.html 文件
    /**
     * @type {import('html-webpack-plugin').HtmlWebpackPlugin.Options}
     */
    new HtmlWebpackPlugin({
      filename: 'index.html',
      // 或者写 path.resolve(__dirname, '../public/index.html')
      template: 'public/index.html',
      title: '模版'
    }),

    new webpack.DefinePlugin({
      'process.env': JSON.stringify(loadEnv())
    })
  ],

  // 别名
  resolve: {
    alias: {
      '@': path.resolve(__dirname, '../src')
    },
    extensions: ['.js', '.vue', '.json']
  },

  module: {
    // 不解析所有压缩过的js文件，避免性能损耗
    noParse: [/\.min\.js$/],
    rules: [
      {
        test: /\.(?:js|mjs|cjs)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          /**
           * @type {import('@babel/core').TransformOptions}
           */
          options: {
            // 增加缓存
            cacheDirectory: true
          }
        }
      },
      {
        test: /\.vue$/,
        use: 'vue-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif|webp)$/i,
        type: 'asset/resource',
        exclude: /node_modules/
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: 'asset/resource',
        // 在打包的时候，放在fonts目录下
        generator: {
          filename: 'fonts/[name][ext]'
        },
        exclude: /node_modules/
      }
    ]
  }
}
