// 通用
const commonCDN = [
  "https://cdn.bootcdn.net/ajax/libs/axios/1.5.0/axios.min.js",
]

// vue2
const v2 = [
  'https://cdn.bootcdn.net/ajax/libs/vue/2.7.9/vue.runtime.common.prod.min.js',
  "https://cdn.bootcdn.net/ajax/libs/vue-router/3.6.5/vue-router.min.js",
  "https://cdn.bootcdn.net/ajax/libs/vuex/3.6.2/vuex.min.js",
  "https://cdn.bootcdn.net/ajax/libs/element-ui/2.15.14/index.min.js"
]

// vue3
const v3 = [

]

const css = [
  'https://cdn.bootcdn.net/ajax/libs/element-ui/2.15.14/theme-chalk/index.min.css'
]

module.exports = {
  v2Merge: commonCDN.concat(v2),
  v3Merge: commonCDN.concat(v3),
  css
}
