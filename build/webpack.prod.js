const { merge } = require('webpack-merge');
const path = require('path')
const fs = require('fs')

const baseConf = require('./webpack.common')

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')

// 分析依赖
const { BundleStatsWebpackPlugin } = require('bundle-stats-webpack-plugin');

const isAnalyzer = process.env.analyzer

// cdn等配置文件
const cdnConfig = require('./config')

/**
 * @type {import('webpack').WebpackPluginInstance}
 */
const plugins = [
  // 生产环境分离css为单独的样式文件
  new MiniCssExtractPlugin(
    {
      filename: 'css/[name].[fullhash:8].css'
    }
  )
]

// 解析项目配置
try {
  fs.accessSync('./project.config.js')
  // require基于当前目录， 上面读取文件基于项目目录
  const projectConfig = require('../project.config.js')

  // 动态插入cdn
  /**
    * @type {import('html-webpack-plugin').HtmlWebpackPlugin.Options}
    */
  // const htmlPlugin = new HtmlWebpackPlugin({
  //   filename: 'index.html',
  //   // 或者写 path.resolve(__dirname, '../public/index.html')
  //   template: 'public/index.html',
  //   title: '模版',
  // })

  // plugins.push(htmlPlugin)
} catch (error) {
  console.log(error);
  // 没有配置文件，就什么都不处理
}


if (isAnalyzer) {
  plugins.push(
    new BundleStatsWebpackPlugin()
  )
}

/**
 * @type {import('webpack').Configuration}
 */
const prodConf = {
  mode: 'production',
  output: {
    filename: 'js/[name].[fullhash:8].js',
    path: path.resolve(__dirname, '../dist'),
    // 自定清理dist的文件
    clean: true,

    // 打包的时候，图片单独输出
    assetModuleFilename: 'images/[name].[fullhash:8].[ext]'
  },

  plugins,
  module: {
    rules: [
      {
        test: /\.css/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader']
      },
      {
        test: /\.scss/i,
        // 生产环境，将样式使用link外链
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader'],
        exclude: /node_modules/
      },
      {
        test: /\.less/i,
        // 生产环境，将样式使用link外链
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'less-loader'],
        exclude: /node_modules/
      }
    ]
  },
  /**
   * 优化
   */
  optimization: {
    // 调试下面的 splitChunks.cacheGroups chunk分组是否是期望效果， minimize 设置为 false ，就不要引入minimizer
    // minimize: false,

    minimize: true,

    // 增加自定义配置
    minimizer: [
      new TerserPlugin({
        parallel: true,
        terserOptions: {
          compress: {
            // 移除console.*
            drop_console: true,
            // 移除debugger
            drop_debugger: true
          }
        }
      })
    ],
    splitChunks: {
      cacheGroups: {
        // 第三方包
        // vendors: {
        //   test: /[\\/]node_modules[\\/]/,
        //   name: 'vendors',
        //   // https://webpack.docschina.org/plugins/split-chunks-plugin/#splitchunkschunks
        //   chunks: 'all',
        //   priority: 20,
        //   enforce: true
        // }

        vue_vendors: {
          test: /[\\/]node_modules[\\/](vue|vue-router|vuex)[\\/]/,
          name: 'vue_vendors',
          chunks: 'all',
          priority: 20,
          enforce: true
        },
        common_vendors: {
          test: /[\\/]node_modules[\\/]/,
          name: 'common_vendors',
          chunks: 'all',
          priority: 10,
          enforce: true
        }
      }
    }
  },
  // CDN
  // externals: {
  //   // 排除一些包，不会打包进 bundle中
  //   // 左侧为我们在业务中引入的包名， 右侧 为对应库提供给外部引用的名字
  //   "vue": "Vue",
  //   "vue-router": "VueRouter",
  //   "vuex": "Vuex",
  //   "element-ui": "ELEMENT",
  //   "axios": "axios",
  // }
}

exports.default = merge(baseConf.default, prodConf)