const { merge } = require('webpack-merge')

const baseConf = require('./webpack.common')

/**
 * @type {import('webpack').Configuration}
 */
const devConf = {
  mode: 'development',

  /**
   * @type {import('webpack-dev-server').Configuration}
   */
  devServer: {
    // 处理vue-router为 history 模式，路由刷新会丢失的问题
    historyApiFallback: true,
    client: {
      // 禁止webpack-dev-server在 遇到错误（http 404, 编译错误，代码错误等）覆盖一个全局的红色弹窗
      overlay: {
        // 编译错误-会提示红色弹窗警告
        errors: true,
        // 运行错误，禁止红色弹窗警告
        runtimeErrors: false,
        // 警告-不要提示
        warnings: false
      }
    },
    static: 'dist',
    port: 5001,
    hot: true,
    proxy: {
      '/api': {
        target: 'https://oa-uat1.wsecar.com',
        changeOrigin: true,
        secure: false,
        logLevel: 'debug'
      }
    }
  },

  devtool: 'eval-cheap-module-source-map',

  module: {
    rules: [
      {
        test: /\.css$/i,
        // 开发环境，将样式插入到head标签内
        use: ['style-loader', 'css-loader', 'postcss-loader']
      },
      {
        test: /\.scss$/i,
        // 开发环境，将样式插入到head标签内
        use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader']
      },
      {
        test: /\.less$/i,
        // 开发环境，将样式插入到head标签内
        use: ['style-loader', 'css-loader', 'postcss-loader', 'less-loader']
      }
    ]
  },

  optimization: {
    runtimeChunk: true,
    removeAvailableModules: false,
    // 开发环境不移除空的chunk，移除本身也是算法计算出来的，项目大也会损耗性能
    removeEmptyChunks: false,
    // 开发环境不拆包
    splitChunks: false
  }
}

exports.default = merge(baseConf.default, devConf)
