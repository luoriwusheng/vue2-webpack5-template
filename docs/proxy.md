# 本地代理

```js
{
    mode: 'development',
    devServer: {
        proxy: {
            '/api': {
                target: 'https://oa-uat1.wsecar.com',
                changeOrigin: true,
                secure: false,
                logLevel: 'debug'
            }
        }
    }
}
```
